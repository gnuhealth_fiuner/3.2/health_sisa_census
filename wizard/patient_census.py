# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
import sys

from datetime import date

from ..census_ws import CensusWS
from trytond.modules.health_sisa_puco.puco_ws import PucoWS

from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval, Bool, Not

__all__ = ['PatientCensusDataStart', 'PatientCensusData']


class PatientCensusDataStart(ModelView):
    'Patient Census Data Start'
    __name__ = 'patient.census_data.start'

    identificado_renaper = fields.Char('RENAPER code', readonly=True)#
    padron_sisa = fields.Char('SISA census', readonly=True)#
    codigo_sisa = fields.Char('SISA code', readonly=True)#
    tipo_documento = fields.Char('ID type', readonly=True)#
    nro_documento = fields.Char('ID number', readonly=True)#
    apellido = fields.Char('Lastname', readonly=True)
    nombre = fields.Char('Name', readonly=True)
    sexo = fields.Char('Sex', readonly=True)
    fecha_nacimiento = fields.Date('Birth date', readonly=True)
    estado_civil = fields.Char('Marital status', readonly=True)
    provincia = fields.Char('Province', readonly=True)
    departamento = fields.Char('Subdivision', readonly=True)
    localidad = fields.Char('City', readonly=True)
    domicilio = fields.Char('Address', readonly=True)
    piso_depto = fields.Char('Apartment', readonly=True)
    codigo_postal = fields.Char('Zip code', readonly=True)
    pais_nacimiento = fields.Char('Birth country', readonly=True)
    provincia_nacimiento = fields.Char('Birth province', readonly=True)
    localidad_nacimiento = fields.Char('Birth city', readonly=True)
    nacionalidad = fields.Char('Nationality', readonly=True)
    fallecido = fields.Char('Deceased', readonly=True)
    fecha_fallecido = fields.Date('Fecha de Deceso', readonly=True)
    cobertura = fields.One2Many('get.socialsecurity.view',None,'Cobertura',
        readonly=True)
    obra_social_elegida = fields.Many2One('party.party','Obra Social Elegida',                       
        domain=[('id', 'in', Eval('obra_social_domain'))])
    obra_social_domain = fields.Function(
        fields.Many2Many('party.party',None, None, 'Obra social Domain'),
                    'on_change_with_obra_social_domain')
    url_padron = fields.Char(u'Buscar en el padron web', readonly=True)
    select_du = fields.Selection([
        ('',''),
        ('yes',u'Sí'),
        ('no','No'),
        ],'Utilizar ReNaPer para UD',
        help=u'Utilizar los datos provistos por\n'
        u'ReNaPer para la Unidad Domiciliaria',required=True)
    
    def get_obra_social_domain(self, name=None):
        Party = Pool().get('party.party')
        rnos_list = [c.rnos for c in self.cobertura]          
        obras_sociales = Party.search([
            ('is_insurance_company','=',True),
            ('identifiers.code','in', rnos_list),
            ])
        nombreObraSocial = [c.nombreObraSocial for c in self.cobertura]
        obras_sociales+= Party.search([
            ('is_insurance_company','=',True),
            ('name','in',nombreObraSocial)])
        obras_sociales = list(set(obras_sociales))
        return [os.id for os in obras_sociales]

    @fields.depends('cobertura')
    def on_change_with_obra_social_domain(self, name=None):
        Party = Pool().get('party.party')
        rnos_list = [c.rnos for c in self.cobertura]
        obras_sociales = Party.search([
            ('is_insurance_company','=',True),
            ('identifiers.code','in', rnos_list),
            ])
        nombreObraSocial = [c.nombreObraSocial for c in self.cobertura]
        obras_sociales += Party.search([
            ('is_insurance_company','=',True),
            ('name','in',nombreObraSocial)])
        obras_sociales = list(set(obras_sociales))
        return [os.id for os in obras_sociales]
    
    @staticmethod
    def default_url_padron():
        return 'www.saludnqn.gob.ar/PadronConsultasWeb/'


class PatientCensusData(Wizard):
    'Patient Census Data'
    __name__ = 'patient.census_data'

    @classmethod
    def __setup__(cls):
        super(PatientCensusData, cls).__setup__()
        cls._error_messages.update({
            'more_than_one_patient': 'Please, select only one patient',
            'error_ws': 'SISA web service error',
            'error_autenticacion': 'User authentication error',
            'error_inesperado': 'Unexpected error',
            'no_cuota_disponible': 'User has no asigned quota',
            'error_datos': 'Remote call error',
            'registro_no_encontrado': 'No data found',
            'multiple_resultado': 'More than one result',
            'servicio_no_disponible': 'Service not avaible.\
                            Try defining the person gender',
        })

    start = StateView(
        'patient.census_data.start',
        'health_sisa_census.patient_census_data_start_view', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'update_patient', 'tryton-ok', default=True),
        ])
    update_patient = StateTransition()

    def default_start(self, fields):
        Patient = Pool().get('gnuhealth.patient')
        res = {}
        if len(Transaction().context['active_ids']) > 1:
            self.raise_user_error('more_than_one_patient')
        patient = Patient(Transaction().context['active_id'])
        if not patient.name:
            return res
        
        xml = None
        if patient.name.gender:
            xml = CensusWS.get_xml(patient.name.ref,patient.name.gender)
        else:
            xml = CensusWS.get_xml(patient.name.ref)
        xml_puco = PucoWS.get_xml(patient.name.ref)
        if xml is None:
            self.raise_user_error('error_ws')
        if xml.findtext('resultado') == 'OK':
            cobertura = []
            osocial = {}
            #coberturas encontradas en el padron ciudadano
            for cober in xml.findall('cobertura'):
                osocial['tipoCobertura'] = cober.findtext('tipoCobertura')
                osocial['nombreObraSocial'] = cober.findtext('nombreObraSocial')
                osocial['rnos']= cober.findtext('rnos')                
                osocial['vigenciaDesde '] = cober.findtext('vigenciaDesde')
                osocial['fechaActualizacion'] = cober.findtext('fechaActualizacion')
                osocial['procedencia'] = 'padron'
                cobertura.append(osocial)
                osocial = {}
            #coberturas encontradas en puco
            if xml_puco and xml_puco.findtext('resultado') == 'OK':
                for cober in xml_puco.findall('puco'):
                    osocial['nombreObraSocial'] = cober.findtext('coberturaSocial')
                    osocial['rnos'] = cober.findtext('rnos')
                    osocial['denominacion'] = cober.findtext('denominacion')
                    osocial['procedencia'] = 'puco'                        
                    cobertura.append(osocial)
                    osocial = {}
            res = {
                'identificado_renaper': xml.findtext('identificadoRenaper'),
                'padron_sisa': xml.findtext('PadronSISA'),
                'codigo_sisa': xml.findtext('codigoSISA'),
                'tipo_documento': xml.findtext('tipoDocumento'),
                'nro_documento': xml.findtext('nroDocumento'),
                'apellido': xml.findtext('apellido'),
                'nombre': xml.findtext('nombre'),
                'sexo': xml.findtext('sexo'),
                'fecha_nacimiento': xml.findtext('fechaNacimiento'),
                'estado_civil': xml.findtext('estadoCivil'),
                'provincia': xml.findtext('provincia'),
                'departamento':xml.findtext('departamento'),
                'localidad': xml.findtext('localidad'),
                'domicilio': xml.findtext('domicilio'),
                'piso_depto': xml.findtext('pisoDpto'),
                'codigo_postal': xml.findtext('codigoPostal'),
                'pais_nacimiento': xml.findtext('paisNacimiento'),
                'provincia_nacimiento': xml.findtext('provinciaNacimiento'),
                'localidad_nacimiento': xml.findtext('localidadNacimiento'),
                'nacionalidad': xml.findtext('nacionalidad'),
                'fallecido': xml.findtext('fallecido'),
                'fecha_fallecido': xml.findtext('fechaFallecido'),                
                'cobertura': cobertura,
            }
        elif xml.findtext('resultado') == 'ERROR_AUTENTICACION':
            self.raise_user_error('error_autenticacion')
        elif xml.findtext('resultado') == 'ERROR_INESPERADO':
            self.raise_user_error('error_inesperado')
        elif xml.findtext('resultado') == 'NO_TIENE_QUOTA_DISPONIBLE':
            self.raise_user_error('no_cuota_disponible')
        elif xml.findtext('resultado') == 'ERROR_DATOS':
            self.raise_user_error('error_datos')
        elif xml.findtext('resultado') == 'REGISTRO_NO_ENCONTRADO':
            self.raise_user_error('registro_no_encontrado')
        elif xml.findtext('resultado') == 'SERVICIO_RENAPER_NO_DISPONIBLE':
            self.raise_user_error('servicio_no_disponible')
        elif xml.findtext('resultado') == 'MULTIPLE_RESULTADO':
            self.raise_user_error('multiple_resultado')

        return res

    def transition_update_patient(self):
        pool = Pool()
        Party = pool.get('party.party')
        Address = pool.get('party.address')
        Country = pool.get('country.country')
        Insurance = pool.get('gnuhealth.insurance')
        Patient = pool.get('gnuhealth.patient')
        
        patient = Patient(Transaction().context['active_id'])
        party = patient.name
        
        identifier_data = {
            'type': 'ar_dni' if self.start.tipo_documento == 'DNI' else None,
            'code': self.start.nro_documento,
            }
        
        party.name = self.start.nombre
        party.lastname = self.start.apellido
        party.ref = self.start.nro_documento
        if not party.identifiers:
            party.identifiers+= (identifier_data),
        party.dob = self.start.fecha_nacimiento
        party.gender = self.start.sexo.lower() if self.start.sexo else 'm'
        party.identified_renaper = True
        party.non_identified_renaper = False
        party.last_sisa_census_check = date.today()
        party.renaper_id = self.start.identificado_renaper
        party.is_person = True
        party.is_patient = True
        
        if self.start.padron_sisa == 'SI':
            party.identified_sisa = True
            party.sisa_code = self.start.codigo_sisa
        if 'trytond.modules.party_ar' in sys.modules:
            party.iva_condition = 'consumidor_final'

        if self.start.pais_nacimiento:
            country = Country.search(
                ['name', 'ilike', self.start.pais_nacimiento]
                )
            if country:
                party.citizenship = country[0].id

        if self.start.domicilio:
            direccion = Address().search([
                ('party', '=', party.id),
                ])
            if direccion and (direccion[0].street is None
                    or direccion[0].street == ''):
                direccion[0].update_direccion(party, self.start)
            else:
                direccion = Address()
                direccion.update_direccion(party, self.start)

        insurance = None
        if self.start.obra_social_elegida:
            insurance_party = Party().search([
                ('id', '=', self.start.obra_social_elegida.id),                
                ])[0]
            if insurance_party:
                insurance = Insurance().search([
                    ('name', '=', party.id),
                    ('company', '=', insurance_party.id),
                    ])
                if not insurance:
                    insurance_data = {
                        'name': party.id,
                        'number': self.start.nro_documento,
                        'company': insurance_party.id,
                        'insurance_type':
                            insurance_party.insurance_company_type,
                        }
                    insurance = Insurance.create([insurance_data])
            if insurance:
                if not patient.current_insurance:
                    patient.current_insurance = insurance[0]
                    patient.save()

        #insurance_party = Party().search([
            #('is_insurance_company', '=', True),
            #('identifiers.type', '=', 'ar_rnos'),
            #('identifiers.code', '=', self.start.rnos),
            #])
        #if not insurance_party:
            #insurance_party = Party().search([
                #('is_insurance_company', '=', True),
                #('name', '=', self.start.obra_social_elegida),
                #])

        #if insurance_party:
            #insurance = Insurance().search([
                #('name', '=', party.id),
                #('company', '=', insurance_party.id),
                #])
            #if not insurance:
                #insurance_data = {
                    #'name': party.id,
                    #'number': numero,
                    #'company': insurance_party.id,
                    #'insurance_type':
                        #insurance_party.insurance_company_type,
                    ##'member_since': data.vigencia_os,
                    #}
                #insurance = Insurance.create([insurance_data])
            #else:
                #insurance[0].number = numero
                #insurance[0].company = insurance_party.id
                #insurance[0].insurance_type = insurance_party.insurance_company_type
                ##insurance[0].member_since = self.start.vigencia_os
                #insurance[0].save()
        
        party.deceased = True if self.start.fallecido == 'SI' else False
        party.date_of_deceased = self.start.fecha_fallecido
        party.save()

        return 'end'
